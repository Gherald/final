package com.example.games;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button tictactoe = findViewById(R.id.TicTacToe);
        tictactoe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,TicTacToe.class);
                startActivity(intent);
            }
        });


        Button dice = findViewById(R.id.Dice);
        dice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ActivityDiceRoll.class);
            startActivity(intent);
            }
        });

        Button Bottle = findViewById(R.id.Bottle);
        Bottle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Bottle.class);
                startActivity(intent);
            }
        });


    }
}
